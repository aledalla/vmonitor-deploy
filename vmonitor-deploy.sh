#!/bin/bash

########## INTRO ############
printf "\n\n             #####################################\n             #                                   #\n             #      V-MONITOR SETUP BY AD        #\n             #                                   #\n             #####################################\n          \n\nVerranno effettuate le seguenti operazioni\n\n1) Installazione Xampp e configurazione con dump importato\n2) Installazione Python3 e librerie necesarie\n3) Installazione dei file core\n4) Implementazione xampp e core come servizio\n\n"
read -p "Premi invio per procedere all'installazione"
############XAMPP ENV#############
wget https://www.apachefriends.org/xampp-files/7.3.12/xampp-linux-x64-7.3.12-0-installer.run -P $HOME/vmonitor_install/resources/
chmod u+x $HOME/vmonitor_install/resources/xampp-linux-x64-7.3.12-0-installer.run
sudo $HOME/vmonitor_install/resources/xampp-linux-x64-7.3.12-0-installer.run

############ WEB FILES ###################

sudo cp -r resources/VMonitor2 /opt/lampp/htdocs/

sudo /opt/lampp/xampp start


############ PYTHON & LIBS ################

sudo apt install python3
sudo apt install python3-pip

pip3 install mysql-connector
pip3 install pyVmomi
pip3 install schedule
pip3 install time
############ CORE FILES ##################
mkdir $HOME/VMonitor
cp resources/runner.py /$HOME/VMonitor/
cp resources/collector.py /$HOME/VMonitor/
cp resources/watchdog.py /$HOME/VMonitor/
cp resources/testdisk.py /$HOME/VMonitor/
cp -r resources/tools $HOME/VMonitor/

########### SERVICES ##########
sudo cp resources/vmonitor.service /etc/systemd/system/
sudo cp resources/xampp.service /etc/systemd/system/
sudo cp resources/testdisk.service /etc/systemd/system/

sudo systemctl daemon-reload

sudo systemctl enable xampp.service
sudo systemctl enable vmonitor.service
sudo systemctl enable testdisk.service

############ IMPORT SQL DUMP ##############
/opt/lampp/bin/mariadb -u root < $HOME/vmonitor_install/resources/vmonitorlast.sql

########### INSTALL GRAFANA #################
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
sudo apt update
sudo apt install grafana
sudo cp resources/grafana.ini /etc/grafana/ ##copia configurazione per accesso anonimo
sudo systemctl daemon-reload
sudo systemctl start grafana-server.service
sudo systemctl enable grafana-server.service

########### GRAPHANA MANUAL IMPORTS ################
printf "\n\nLa configurazione di Grafana deve essere effettuata manualmente\n\n1) collegarsi all'indirizzo localhost sulla porta 3000 - potrebbe passare qualche minuto prima che salga il servizio\n2) autenticarsi come admin/admin\n3) cliccare su add datasource\n4)Scegliere mysql impostando nome database come vmonitor e nome utente come root, usando la password °!pZçv8#§§eW quindi premere save and test, dovrebbe mostrarsi un avviso di successo\n5)dal menu a sinistra cliccare sull'icona aggiungi per poi caricare il file json chiamato Main-1576336051825.json"


########### REMOVE INSTALLATION FILES ############

sudo rm -r /home/vmonitor/vmonitor_install/

########## REBOOT ##########
sudo reboot


