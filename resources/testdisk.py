import shutil,mysql.connector,schedule,time
from datetime import datetime,timedelta

def writeDataToDb(sql):
    mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="°!pZçv8#§§eW",
    database="vmonitor"
    )

    mycursor = mydb.cursor()

    mycursor.execute(sql)
    mydb.commit()


def Execute():
# write disk perentage 
    total, used, free = shutil.disk_usage("/")

    totl = (total // (2**30))
    usd = (used // (2**30))

    percentage = int((usd/totl)*100)

    sql = "insert into system (percentage,timestamp) values ('%s','%s')" % (percentage,datetime.now())
    writeDataToDb(sql)

    # delete > 30 days old records
    tables=['datastore','host_data','network','storage','vm_data','system_sensors']

    past = datetime.now() - timedelta(days=30)

    for x in tables:
        sql = "delete from %s where timestamp < '%s'" % (x,past)
        writeDataToDb(sql)


schedule.every().day.do(Execute)

while True: 
    schedule.run_pending() 
    time.sleep(1) 