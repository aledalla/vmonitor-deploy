<html>
<header>
<script type="text/javascript" src="host_summary/host_summary.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="host_summary/host_summary.css">

</header>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="watchdog.php">Watchdog</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Logout</a>
            </li>
          
        </ul>
    </div>
    <div class="mx-auto order-0">
        <a class="navbar-brand mx-auto" href="index.php">V-Monitor @ Host summary</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="settings.php">Impostazioni</a>
            </li>
            
        </ul>
    </div>
</nav>
<div class="external">
<div class="grid-container" id="external">
    <div class="grid-row">
<div class="grid-item">
<canvas id="cpu"></canvas>
</div>
<div class="grid-item">
<canvas id="memory"></canvas>
</div>
<div class="grid-item">
<canvas id="cpu-des"></canvas>
</div>
</div>
<div class="grid-item">
<canvas id="mem-des"></canvas>
</div>
</div>
<div class="sensors">
</div>
<div class="sensors" id="sensors">
</div>
<div class="sensors" id="disks">
</div>
</div>  

</div>
</body>
</html>

<?php
     $sql = "SELECT name from dashboards where host = '%s'";
     $sql = sprintf($sql,$_GET['server']);
     
     $result = getDataFromDb($sql);
     
     if ($result->num_rows > 0) {
         // output data of each row
         while($row = $result->fetch_assoc()) {
             $dashboards[] = $row['name'];
         }
        
     } else {
         echo "0 results";
     }
    if(in_array('global_cpu',$dashboards)){
    $sql = "SELECT effective_cpu, overall_cpu from host_data where host = '%s' limit 1";
    $sql = sprintf($sql,$_GET['server']);
    
    $result = getDataFromDb($sql);
    
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $data[] = $row['effective_cpu'];
            $data[] = $row['overall_cpu'];
            $labels[] = 'cpu totale';
            $labels[] = 'cpu in uso';
            $colors[] = rand_color();
            $colors[] = rand_color();
        }
        $convData = json_encode($data);
        $labels = json_encode($labels);
    $colors = json_encode($colors);
    $formatted = "<script>testChart('cpu',%s,%s,%s,'Occupazione globale cpu')</script>";
    $formatted = sprintf($formatted,$convData,$labels,$colors);

        echo($formatted);
    } else {
        echo "0 results";
    }
}
if(in_array('global_ram',$dashboards)){
    $sql = "SELECT effective_memory, overall_memory from host_data where host = '%s' limit 1";
    $sql = sprintf($sql,$_GET['server']);
    
    $result = getDataFromDb($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $data2[] = $row['effective_memory'];
            $data2[] = $row['overall_memory'];
            $labels2[] = 'memoria totale';
            $labels2[] = 'memoria in uso dalle vms';
            $colors2[] = rand_color();
            $colors2[] = rand_color();
        }
        $convData2 = json_encode($data2);
            $labels2 = json_encode($labels2);
            $colors2 = json_encode($colors2);
            $formatted2 = "<script>testChart('memory',%s,%s,%s,'Occupazione globale memoria')</script>";
            $formatted2 = sprintf($formatted2,$convData2,$labels2,$colors2);
        echo($formatted2);
    } else {
        echo "0 results";
    }
}
if(in_array('global_cpu_per_vm',$dashboards)){
    $sql = "SELECT c.name,c.cpuusage from esxi_credentials inner join (SELECT * FROM vm_data WHERE timestamp IN ( SELECT MAX(timestamp) FROM vm_data GROUP BY uuid )) as c on c.server = esxi_credentials.Server where c.server = '%s' group by c.uuid";
    $sql = sprintf($sql,$_GET['server']);
    
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $data3[] = $row['cpuusage'];
             $labels3[] = $row['name'];
             $colors3[] = rand_color();
            
        }
        $convData3 = json_encode($data3);
        $labels3 = json_encode($labels3);
        $colors3 = json_encode($colors3);
        $formatted = "<script>testChart('cpu-des',%s,%s,%s,'Distribuzione cpu per vm')</script>";
        $formatted = sprintf($formatted,$convData3,$labels3,$colors3);
   
        echo($formatted);

    } else {
        echo "0 results";
    }
}
if(in_array('global_ram_per_vm',$dashboards)){
    $sql = "SELECT c.name,c.host_memory from esxi_credentials inner join (SELECT * FROM vm_data WHERE timestamp IN ( SELECT MAX(timestamp) FROM vm_data GROUP BY uuid )) as c on c.server = esxi_credentials.Server where c.server = '%s' group by c.uuid";
    $sql = sprintf($sql,$_GET['server']);
    
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $data4[] = $row['host_memory'];
             $labels4[] = $row['name'];
             $colors4[] = rand_color();
            
        }
        $convData4 = json_encode($data4);
        $labels4 = json_encode($labels4);
        $colors4 = json_encode($colors4);
        $formatted = "<script>testChart('mem-des',%s,%s,%s,'Distribuzione ram per vm')</script>";
        $formatted = sprintf($formatted,$convData4,$labels4,$colors4);
   
        echo($formatted);

    } else {
        echo "0 results";
    }
}
if(in_array('global_datastores',$dashboards)){
    $sql = "SELECT c.name,c.freespace,c.provisioned from esxi_credentials inner join (SELECT * FROM datastore WHERE timestamp IN ( SELECT MAX(timestamp) FROM datastore GROUP BY name )) as c on c.host = esxi_credentials.Server where c.host = '%s' group by c.name";
    $sql = sprintf($sql,$_GET['server']);
    
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
        $counter = 1;
        // output data of each row
       
        while($row = $result->fetch_assoc()) {
            $data4 = array();
            $labels4 = array();
            $colors4 = array();
            $formatted = "<script>addChart('%s',%s,)</script>";
            $formatted = sprintf($formatted,'grid-item',$counter); #CAMBIARE CLASSE IN MODO INTELLIGENTE, INTANTO FIXATA SU LEFT
        
            echo($formatted);
     
            $data4[] = $row['freespace'];
            $data4[] = $row['provisioned'];
             $labels4[] = 'Spazi rimanente';
             $labels4[] = 'Spazi occupato';
             $colors4[] = rand_color();
             $colors4[] = rand_color();
             $convData4 = json_encode($data4);
             $labels4 = json_encode($labels4);
             $colors4 = json_encode($colors4);
             $formatted = "<script>testChart('%s',%s,%s,%s,'Utilizzo complessivo datastore %s')</script>";
             $formatted = sprintf($formatted,$counter,$convData4,$labels4,$colors4,$row['name']);
        
             echo($formatted);
             $counter = $counter + 1;
        }
        
    } else {
        echo "0 results";
    }
}
//IMPLEMENTARE DATASTORE USAGE PER VM
/* if(in_array('global_datastores_per_vm',$dashboards)){
    $sql = "SELECT c.name,c.storage from esxi_credentials inner join (SELECT * FROM data WHERE timestamp IN ( SELECT MAX(timestamp) FROM data GROUP BY uuid )) as c on c.server = esxi_credentials.Server where c.server = '%s' group by c.uuid";
    $sql = sprintf($sql,$_GET['server']);
    
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
        $counter = 1;
        // output data of each row
       $data4 = array();
       $labels4 = array();
       $colors4 = array();
        while($row = $result->fetch_assoc()) {
            $data4[] = $row['storage'];
             $labels4[] = $row['name'];
             $colors4[] = rand_color();
            
        }
        $convData4 = json_encode($data4);
        $labels4 = json_encode($labels4);
        $colors4 = json_encode($colors4);
        $formatted = "<script>testChart('',%s,%s,%s,'Distribuzione ram per vm')</script>";
        $formatted = sprintf($formatted,$convData4,$labels4,$colors4);
   
        echo($formatted);
        
    } else {
        echo "0 results";
    }
} */

    $sql = "SELECT c.name, c.status from esxi_credentials inner join (SELECT * FROM storage WHERE timestamp IN ( SELECT MAX(timestamp) FROM storage GROUP BY name )) as c on c.host = esxi_credentials.Server WHERE c.host = '%s' GROUP BY c.name";
    $sql = sprintf($sql,$_GET['server']);
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
        
        // output data of each row
       $everythingOk = true;
        while($row = $result->fetch_assoc()) {
            if($row['status'] != 'Green'){
                $everythingOk = false;
                $formatted = "<script>reportedDisks('%s','%s')</script>";
                $formatted = sprintf($formatted,$row['name'],$row['status']);
                echo($formatted);
                
            }
        }
        if($everythingOk == True){
            echo("<script>reportedDisks()</script>");
                
        }
    } else {
        echo "0 results";
    }
    $sql = "SELECT c.name,reading,type,temp_limit from esxi_credentials inner join (SELECT * FROM system_sensors WHERE timestamp IN ( SELECT MAX(timestamp) FROM system_sensors GROUP BY name )) as c on c.host = esxi_credentials.Server INNER JOIN registered_sensors ON registered_sensors.name = c.name WHERE c.host = '%s' GROUP BY c.name";
    $sql = sprintf($sql,$_GET['server']);
   
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
        
        // output data of each row
       $everythingOk = true;
        while($row = $result->fetch_assoc()) {
            if($row['type'] == 'temperature' and $row['temp_limit'] < $row['reading']){
                $everythingOk = false;
                $formatted = "<script>reportedSensors('%s')</script>";
                $formatted = sprintf($formatted,$row['name'] . " ha superato il limite di allerta e la sua temperatura attuale è di " . $row['reading'] . " gradi");
                echo($formatted);
            }
            if($row['type'] == 'power' and $row['reading'] == 0){
                $everythingOk = false;
                $formatted = "<script>reportedSensors('%s')</script>";
                $formatted = sprintf($formatted,$row['name'] . "risulta non connesso");
                echo($formatted);
            }
            if($row['type'] == 'fan' and $row['reading'] == 0){
                $everythingOk = false;
                $formatted = "<script>reportedSensors('%s')</script>";
                $formatted = sprintf($formatted,$row['name'] . "risulta non sta funzionando");
                echo($formatted);
            }
        }
        if($everythingOk == true){
            echo("<script>reportedSensors()</script>");
        }
    } else {
        echo "0 results";
    }
    function rand_color() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

function getDataFromDb($sql){
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "vmonitor";
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        echo("errore durante la connessione al db");
        die("Connection failed: " . $conn->connect_error);
    }
    $result = $conn->query($sql);
    return $result;
    $conn->close();
}
?>