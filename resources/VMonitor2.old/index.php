<html>
    <header>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="index/index.js"></script>
        <body>
        
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="watchdog.php">Watchdog</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Logout</a>
            </li>
          
        </ul>
    </div>
    <div class="mx-auto order-0">
        <a class="navbar-brand mx-auto" href="index.php">V-Monitor @ Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="settings.php">Impostazioni</a>
            </li>
            
        </ul>
    </div>
</nav>
<div class="d-flex justify-content-center align-items-center" id="semi-ex" Style="width:70%; height:100%; margin:auto;">


</div>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "vmonitor";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT * FROM esxi_credentials";
$result = mysqli_query($conn, $sql);


if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        //$formatted = "<script>generateVSphere('%s','%s')</script>";
        //echo sprintf($formatted,$row['Server'],'none');
        $hosts[] = $row['Server'];
    }
} else {
    #
}

mysqli_close($conn);





for ($x = 0; $x <= count($hosts)-1; $x++) {
    $statusGreen = true;
    $statusYellow = false;
    $statusRed = false;

    $sql = "SELECT c.name, c.status from esxi_credentials inner join (SELECT * FROM storage WHERE timestamp IN ( SELECT MAX(timestamp) FROM storage GROUP BY name )) as c on c.host = esxi_credentials.Server WHERE c.host = '%s' GROUP BY c.name";
    $sql = sprintf($sql,$hosts[$x]);
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
        
        // output data of each row
        while($row = $result->fetch_assoc()) {
            if($row['status'] == 'Yellow'){
                $statusYellow = true;
            }
            if($row['status'] == 'Red'){
                $statusRed = true;
            }
        }
        
    } else {
        echo "0 results";
    }




    $sql = "SELECT c.name,reading,type,temp_limit from esxi_credentials inner join (SELECT * FROM system_sensors WHERE timestamp IN ( SELECT MAX(timestamp) FROM system_sensors GROUP BY name )) as c on c.host = esxi_credentials.Server INNER JOIN registered_sensors ON registered_sensors.name = c.name WHERE c.host = '%s' GROUP BY c.name";
    $sql = sprintf($sql,$hosts[$x]);
   
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
   
        while($row = $result->fetch_assoc()) {
            if($row['type'] == 'temperature' and $row['temp_limit'] < $row['reading']){
                $statusYellow = true;
            }
            if($row['type'] == 'power' and $row['reading'] == 0){
                $statusYellow = true;
            }
            if($row['type'] == 'fan' and $row['reading'] == 0){
                $statusYellow = true;
            }
        }
       
    } else {
        echo "0 results";
    }

    $formatted = "<script>generateVSphere('%s','%s')</script>";
    if($statusRed == true){
        echo sprintf($formatted,$hosts[$x],'Red');
    }
    else if($statusYellow == true){
        echo sprintf($formatted,$hosts[$x],'Yellow');
    }
    else{
        echo sprintf($formatted,$hosts[$x],'Green');
    }
    
}






    
    function getDataFromDb($sql){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "vmonitor";
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            echo("errore durante la connessione al db");
            die("Connection failed: " . $conn->connect_error);
        }
        $result = $conn->query($sql);
        return $result;
        $conn->close();
    }
?>

        </body>
    </header>
</html>