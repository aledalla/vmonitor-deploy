function populateSensorsList(name){
var x = document.getElementById("sel1");
var option = document.createElement("option");
option.text = name;
x.add(option)
}
function populateSensorsRemove(name){
    var x = document.getElementById("selremove");
    var option = document.createElement("option");
    option.text = name;
    x.add(option)
}

function selected(type){
    
    if(type=='sensor'){
        
        if(document.getElementById('btn1')!=undefined){
            document.getElementById('btn1').remove()
        }
        if(document.getElementById('x')!=undefined){
            document.getElementById('x').remove()
        }
        if(document.getElementById('y')!=undefined){
            document.getElementById('y').remove()
        }
        var seeker = document.createElement('input')
        seeker.setAttribute('type','range')
        seeker.setAttribute('id','x')
        seeker.setAttribute('class','custom-range')

        var e = document.getElementById("sel1");
        var strUser = e.options[e.selectedIndex].text;
        var label = document.createElement('label')
        label.setAttribute('for','x')
        label.setAttribute('id','y')
        label.innerText = "Seleziona temperatura di allarme"
        if(strUser.includes('Temp')){
            seeker.setAttribute('min','0')
            seeker.setAttribute('max','150')
            seeker.setAttribute('name','seeker')
            document.getElementById('sensors').appendChild(label)
            document.getElementById('sensors').appendChild(seeker)
        }
        var btn1 = document.createElement('button')
        btn1.setAttribute('id','btn1')
        btn1.setAttribute('type','submit')
        btn1.innerText='Aggiungi'
        btn1.setAttribute('class','btn btn-primary')
        document.getElementById('sensors').appendChild(btn1)
        
    }
    
    if(type=='sensorremove'){
        if(document.getElementById('btnSensorRemove')!=undefined){
            document.getElementById('btnSensorRemove').remove()
        }
        var btnrmSensor = document.createElement('button')
        btnrmSensor.setAttribute('id','btnSensorRemove')
        btnrmSensor.setAttribute('type','submit')
        btnrmSensor.innerText = "Rimuovi"
        btnrmSensor.setAttribute('class','btn btn-danger')
        document.getElementById('sensorremove').appendChild(btnrmSensor)

    }
   
}
function setChecked(name){
        //console.log(name)
     document.getElementById(name).checked = true
    
}
function handleClick(name){
   
    var space = document.createElement('br')
    space.setAttribute('id','br')
    var del = document.getElementsByName('br')
    if(document.getElementById('br')!=undefined){
    document.getElementById('br').remove()
    }

        if(document.getElementById('newLabel')){
            document.getElementById('newLabel').remove()
            document.getElementById('newInput').remove()
            document.getElementById('setSingle').remove()
            document.getElementById('labelDelay').remove()
            document.getElementById('inputDelay').remove()
        }
        if(document.getElementById('newLabel0')){
        for (i = 0; i <= 2; i++) {
            document.getElementById('newLabel'+i).remove()
            document.getElementById('newInput'+i).remove()
            
        }
        document.getElementById('labelDelay').remove()
        document.getElementById('inputDelay').remove()
        document.getElementById('setMulti').remove()
    }
    if(name=='multi'){
        names=['storage','hardware','risorse']
        for (i = 0; i <= 2; i++) {
            if(i==0){
                
                document.getElementById('test').appendChild(space)
            }
            var label = document.createElement('label')
            label.innerText = 'indirizzamento alert ' + names[i]
            label.setAttribute('id','newLabel'+i)
            label.setAttribute('for','newInput'+i)

            var input = document.createElement('input')
            input.setAttribute('name',names[i])
            input.setAttribute('type','email')
            input.setAttribute('id','newInput'+i)
            input.setAttribute('class','form-control')
            
            document.getElementById('test').appendChild(label)
            document.getElementById('test').appendChild(input)
            }

            var labelDelay = document.createElement('label')
            labelDelay.innerText = 'intervallo di notifica (espresso in ore)'
            labelDelay.setAttribute('id','labelDelay')
            labelDelay.setAttribute('for','inputDelay')
    
            var inputDelay = document.createElement('input')
            inputDelay.setAttribute('type','text')
            inputDelay.setAttribute('id','inputDelay')
            inputDelay.setAttribute('name','delay')
            inputDelay.setAttribute('class','form-control')
    
        var button = document.createElement('button')
        button.setAttribute('id','setMulti')
        button.innerText='Salva'
        button.setAttribute('class','btn btn-primary')
        button.setAttribute('type','submit')
        
        document.getElementById('test').appendChild(labelDelay)
        document.getElementById('test').appendChild(inputDelay)
        document.getElementById('test').appendChild(button)

    }
    if(name=='single'){
        
        var label = document.createElement('label')
        label.innerText = 'indirizzo'
        label.setAttribute('id','newLabel')
        label.setAttribute('for','newInput')

        var input = document.createElement('input')
        input.setAttribute('type','email')
        input.setAttribute('id','newInput')
        input.setAttribute('name','single')
        input.setAttribute('class','form-control')

        var labelDelay = document.createElement('label')
        labelDelay.innerText = 'intervallo di notifica (espresso in ore)'
        labelDelay.setAttribute('id','labelDelay')
        labelDelay.setAttribute('for','inputDelay')

        var inputDelay = document.createElement('input')
        inputDelay.setAttribute('type','text')
        inputDelay.setAttribute('id','inputDelay')
        inputDelay.setAttribute('name','delay')
        inputDelay.setAttribute('class','form-control')

        var button = document.createElement('button')
        button.setAttribute('id','setSingle')
        button.innerText='Salva'
        button.setAttribute('class','btn btn-primary')
        button.setAttribute('type','submit')

        document.getElementById('test').appendChild(space)
        document.getElementById('test').appendChild(label)
        document.getElementById('test').appendChild(input)
        document.getElementById('test').appendChild(labelDelay)
        document.getElementById('test').appendChild(inputDelay)
        document.getElementById('test').appendChild(button)
    }
}