function generateVSphere(name, uuid, powered, status){



    var fatDiv = document.createElement("div");
    fatDiv.setAttribute('class','card')
    fatDiv.setAttribute('style','width: 18rem;height:400px;')
    
    var p = document.createElement('p');
    p.setAttribute('class','card-text');
   


    var img = document.createElement("img")
    if(powered == 'poweredOff'){
        img.setAttribute('src','img/serveroff.svg')
        p.innerText = 'Spenta'
    }
    else if(status=='green'){
        img.setAttribute('src','img/serverok.svg')
        p.innerText = 'Tutto ok'
    }
    else if(status=='yellow'){
        img.setAttribute('src','img/serverwarn.svg')
        p.innerText = 'Avviso'
    }
    else if(status=='red'){
        img.setAttribute('src','img/servererr.svg')
        p.innerText = 'Errore'
    }
    img.setAttribute('class','card-img-top')
    
    var body = document.createElement("div");
    body.setAttribute('class','card-body');

    var h = document.createElement("h5");
    h.innerText = name;
    h.setAttribute('class','card-title')

    

    var a = document.createElement("a");
    a.setAttribute('class','btn btn-primary');
    a.innerText = "Check details";
    a.href = "vmdetails.php?id="+uuid;

    fatDiv.appendChild(img)
    fatDiv.appendChild(body)
    body.appendChild(h)
    body.appendChild(p)
    body.appendChild(a)
    document.getElementById('semi-ex').appendChild(fatDiv)
}


