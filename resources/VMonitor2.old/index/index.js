function generateVSphere(ip,status){

    var fatDiv = document.createElement("div");
    fatDiv.setAttribute('class','card')
    fatDiv.setAttribute('style','width: 18rem;height:400px;')
 
    var img = document.createElement("img")
    if(status == 'Green'){
        img.setAttribute('src','img/flag.svg')
    }
    if(status == 'Yellow'){
        img.setAttribute('src','img/yellowflag.svg')
    }
    if(status == 'Red'){
        img.setAttribute('src','img/redflag.svg')
    }
    img.setAttribute('class','card-img-top')
    
    var body = document.createElement("div");
    body.setAttribute('class','card-body');

    var h = document.createElement("h5");
    h.innerText = ip;
    h.setAttribute('class','card-title')

    var p = document.createElement('a');
    p.setAttribute('class','card-text');
    p.innerText = "summary"
    p.href="host_summary.php?server="+ip

    var br = document.createElement('hr')

    var a = document.createElement("a");
    a.setAttribute('class','btn btn-primary');
    a.innerText = "Vm view";
    a.href = "details.php?ip="+ip;

    fatDiv.appendChild(img)
    fatDiv.appendChild(body)
    body.appendChild(h)
    body.appendChild(p)
    body.appendChild(br)
    body.appendChild(a)
    document.getElementById('semi-ex').appendChild(fatDiv)
}


