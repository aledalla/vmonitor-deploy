<html>
    <header>
    <script src="watchdog/watchdog.js"></script>
    <link rel="stylesheet" href="watchdog/watchdog.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</header>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
           
            <li class="nav-item">
                <a class="nav-link" href="#">Logout</a>
            </li>
          
        </ul>
    </div>
    <div class="mx-auto order-0">
        <a class="navbar-brand mx-auto" href="index.php">V-Monitor @ Watchdog Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="settings.php">Impostazioni</a>
            </li>
            
        </ul>
    </div>
</nav>
<div class="external">
    <div class="left">
  
    <div class="section">
        <div class="subtitle">
            <h4>Watchdog sta controllando le seguenti vm</h4>
        </div> 
        <div class="data">
        <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nome</th>
      <th scope="col">Server</th>
      <th scope="col">Ref</th>
    </tr>
  </thead>
  <tbody id="registeredVm">
 
    
  </tbody>
</table>
        </data>
    </div>
    
</div>
<div class="section">
        <div class="subtitle">
            <h4>Watchdog non sta controllando le seguenti vm</h4>
        </div> 
        <div class="data">
        <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nome</th>
      <th scope="col">Server</th>
      <th scope="col">Ref</th>
    </tr>
  </thead>
  <tbody id="unregisteredVm">
  </tbody>
</table>
        </data>
</div>

    </div>
   
</div>
<div class="right">
<div class="section">
        <div class="subtitle">
            <h4>Watchdog sta controllando i seguenti host</h4>
        </div> 
        <div class="data">
        <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Host</th>
      <th scope="col">Ref</th>
    </tr>
  </thead>
  <tbody id="unregisteredHost">
   
    
  </tbody>

        </data>
</div>
</body>
<html>

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "vmonitor";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sqlCheckRegisteredVm = "select vm_data.name, vm_data.uuid, vm_data.server from vm_data where vm_data.uuid in (SELECT * from registered_vm) GROUP by vm_data.uuid order by server, name";
$sqlCheckUnRegisteredVm = "select vm_data.name, vm_data.uuid, vm_data.server from vm_data where vm_data.uuid not in (SELECT * from registered_vm) GROUP by vm_data.uuid order by server, name";
$sqlCheckUnRegisteredHost = "select Server from esxi_credentials";

$resultSqlCheckRegisteredVm = mysqli_query($conn, $sqlCheckRegisteredVm);
$resultSqlCheckUnRegisteredVm = mysqli_query($conn, $sqlCheckUnRegisteredVm);
$resultSqlCheckUnRegisteredHost = mysqli_query($conn, $sqlCheckUnRegisteredHost);


if (mysqli_num_rows($resultSqlCheckUnRegisteredHost) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($resultSqlCheckUnRegisteredHost)) {
        $formatted = "<script>fillTableUnRegisteredHost('%s')</script>";
        echo sprintf($formatted,$row['Server']);
    }
} else {
   #
}
if (mysqli_num_rows($resultSqlCheckRegisteredVm) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($resultSqlCheckRegisteredVm)) {
        $formatted = "<script>fillTableRegisteredVm('%s','%s','%s')</script>";
        echo sprintf($formatted,$row['name'],$row['server'],$row['uuid']);
    }
} else {
   #
}
if (mysqli_num_rows($resultSqlCheckUnRegisteredVm) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($resultSqlCheckUnRegisteredVm)) {
        $formatted = "<script>fillTableUnRegisteredVm('%s','%s','%s')</script>";
        echo sprintf($formatted,$row['name'],$row['server'],$row['uuid']);
    }
} else {
   #
}
mysqli_close($conn);


?>