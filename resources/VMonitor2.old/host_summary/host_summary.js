
function testChart(id,points,labelss,colors,text){
  
var ctx = document.getElementById(id).getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'doughnut',
    options: {
      legend: {
        display: false
        },
      title: {
          display: true,
          text: text
      }
  },
    // The data for our dataset
    data : {
      datasets: [{
          data: points,
          backgroundColor: colors
      }],
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: labelss,
      
  
      
  } 
  
});
}

function addChart(style,id){
  var newDiv = document.createElement('div')
  newDiv.setAttribute('class',style)
  var newCanvas = document.createElement('canvas')
  newCanvas.setAttribute('id',id)
  newDiv.appendChild(newCanvas)
  document.getElementById('external').appendChild(newDiv)
}


function reportedDisks(name,status){
  var newP = document.createElement('p')
  if(name!=undefined && status != undefined){
  newP.innerText = name+' è stato segnalato con lo stato '+status
  if(status=="Yellow"){
    newP.setAttribute('style','background-color:#ecf078;')
  }
  if(status=="Red"){
    newP.setAttribute('style','background-color:#f26755;')
  }
}
else{
  
  newP.innerText = "Tutti i dischi funzionano correttamente"
newP.setAttribute('style','background-color:#6af266')
}

  document.getElementById('disks').appendChild(newP)
}


function reportedSensors(text){
  var newP = document.createElement('p')
  if(text!=undefined){
  newP.innerText = text
  newP.setAttribute('style','background-color:#ecf078;')
  }
  else{
    newP.innerText = "Tutti i sensori configurati riportano soglie rispettate"
newP.setAttribute('style','background-color:#6af266')
  }
  document.getElementById('sensors').appendChild(newP)
}