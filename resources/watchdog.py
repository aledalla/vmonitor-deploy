import mysql.connector,smtplib,ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import mysql.connector
import datetime

disksWarns = []
sensorsWanrns = []
datastoresWarns = []
overcommitted = []
ramOvercommitted = []
spacePercentage = []

class Disk:
    def __init__(self,host,disk,status,mail):
        self.host = host
        self.disk = disk
        self.status = status
        self.mail = mail
    def __str__(self):
        return "%s riporta stato %s su disco %s" % (self.host,self.status,self.disk)

class Sensor:
    def __init__(self,host,sensor,status,kind,mail):
        self.host = host
        self.sensor = sensor
        self.status = status
        self.kind = kind
        self.mail = mail
    def __str__(self):
        if(self.kind == 'temperature'):
            return "%s riporta soglia superata per il sensore %s con temperatura %s" % (self.host,self.sensor,self.status)
        if(self.kind == 'power'):
            return "%s riporta la periferica %s scollegata" % (self.host,self.sensor)
        if(self.kind == 'fan'):
            return "%s riporta soglia violata per il sensore %s con %s" % (self.host,self.sensor,self.status)

class datastore:
    def __init__(self,host,datastore,percent,mail):
        self.host = host
        self.datastore = datastore
        self.percent = percent
        self.mail = mail
    def __str__(self):
        return "%s riporta che il datastore %s ha raggiunto un occupazione del %s " % (self.host,self.datastore,self.percent) + "%"

class Overcommitted:
    def __init__(self,host,datastore,status,when,mail):
        self.host = host
        self.datastore = datastore
        self.status = status
        self.when = when
        self.mail = mail
    def __str__(self):
        if(self.when == 'now'):
            return "%s riporta che il datastore %s è in stato di overcommitted di %s Gb" % (self.host,self.datastore,self.status)
        if(self.when == 'later'):
            return "%s riporta che il datastore %s sarà in stato overcommitted con ulteriori %s Gb" % (self.host,self.datastore,self.status)

class overcommittedRam:
    def __init__(self,host,status,mail):
        self.host = host
        self.status = status
        self.mail = mail
    def __str__(self):
        return "%s riporta stato overcommitted sulla memoria RAM per un valore di %s Gb" % (self.host,self.status)

def writeDataToDb(sql):
    mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="°!pZçv8#§§eW",
    database="vmonitor"
    )

    mycursor = mydb.cursor()

    mycursor.execute(sql)
    mydb.commit()

def getFromDb(sql):
    mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="°!pZçv8#§§eW",
    database="vmonitor"
    )

    mycursor = mydb.cursor()

    mycursor.execute(sql)

    result = mycursor.fetchall()

    return result

def format2mail(kinda):
    temp = ""
    for x in kinda:
       temp+="<li>"+x+"</li>"
    return temp
        
def sendMail(splittedTemp,address):
    port = 587  # For starttls
    smtp_server = "smtp.gmail.com"
    sender_email = "vmonitor.report@gmail.com"
    receiver_email = address
    password = "Ps23HosIrL"

    message = MIMEMultipart("alternative")
    message["Subject"] = "Vmonitor | Error reporting"
    message["From"] = sender_email
    message["To"] = receiver_email


    # Create the plain-text and HTML version of your message
    text = """\
    Hi,
    How are you?
    Real Python has many great tutorials:
    www.realpython.com"""
    html ="<html><body><center><h1>Watchdog ha segnalato i seguenti errori:</h1><ul>%s</ul></center></body></html>" % (format2mail(splittedTemp))
    
    errorN = 0
    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)

    # Create secure connection with server and send email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )

def parsePercentage(percentage):
    if(percentage > 85):
        temp = "Lo spazio a disposizione della macchina VMonitor sta per esaurirsi; valore attuale %s percento" % (percentage)
        spacePercentage.append(temp)

def parseConfiguredRam(configuredRam,totalram,mailingList):
    if(totalram):
        total = totalram[0][0]
        host = totalram[0][1]
        
        ramSum = 0
        for x in configuredRam[0]:
            ramSum = ramSum + x[0]
        
        for key in mailingList[host]:
            address = key[2]
    
        result = total - ramSum
        if(result<0):
            ramOvercommitted.append(overcommittedRam(host,abs(int(result)),address))
    
   
    
    

def parseDisks(disksData,mailingList):   
    for x in disksData:
        for y in x:
            temp=[]
            for z in y:
                temp.append(z)
            if(temp[1]!='Green'):
                for key in mailingList[temp[2]]:
                    address = key[0]                    
                disksWarns.append(Disk(temp[2],temp[0],temp[1],address))
            
def parseSensors(sensorsData,mailingList):
    for x in sensorsData:
        for y in x:
            temp=[]
            for z in y:
                temp.append(z)
            for key in mailingList[temp[4]]:
                address = key[1]
            if(temp[2] == 'temperature'):
                if(temp[3]>temp[1]):
                       
                    sensorsWanrns.append(Sensor(temp[4],temp[0],temp[1],'temperature',address))
            if(temp[2] == 'power'):
                if(temp[1]== 0):
                       
                    sensorsWanrns.append(Sensor(temp[4],temp[0],temp[1],'power',address))
            if(temp[2] == 'fan'):
                if(temp[3]<temp[1]):
                    
                    sensorsWanrns.append(Sensor(temp[4],temp[0],temp[1],'fan',address))

def parseDatastore(datastoresData,mailingList):
    for x in datastoresData:
        for y in x:
            temp=[]
            for z in y:
                temp.append(z)
            for key in mailingList[temp[3]]:
                address = key[2]
            percent = 100 * (temp[2])/(temp[1])
            if(percent > 90):
                datastoresWarns.append(datastore(temp[3],str(temp[0]),str(int(percent)),address))
            if(temp[1]-temp[2] < 0):
                overcommitted.append(Overcommitted(temp[3],str(temp[0]),str(int((temp[2]-temp[1]))),'now',address))
            if(temp[1]-temp[2] < 100 and temp[1]-temp[2] > 0):
                overcommitted.append(Overcommitted(temp[3],(temp[0]),str(int(temp[1]-temp[2])),'later',address))

def split4mail():
    addresses=[]
    for x in disksWarns:
        if(x.mail not in addresses):
            addresses.append(x.mail)
    for x in sensorsWanrns:
        if(x.mail not in addresses):
            addresses.append(x.mail)
    
    for x in datastoresWarns:
        if(x.mail not in addresses):
            addresses.append(x.mail)
    
    for x in ramOvercommitted:
        if(x.mail not in addresses):
            addresses.append(x.mail)

    for x in addresses:
        splittedTemp = []
        for y in disksWarns:
            if(x==y.mail):
                splittedTemp.append(str(y))
        for y in sensorsWanrns:
            if(x==y.mail):
                splittedTemp.append(str(y))
        for y in datastoresWarns:
            if(x==y.mail):
                splittedTemp.append(str(y))
        for y in ramOvercommitted:
            if(x==y.mail):
                splittedTemp.append(str(y))
        for y in spacePercentage:
            splittedTemp.append(y)

        sendMail(splittedTemp,x)

def execute():
    queryStampNotify = "update mailing_list set last_notify = '{0}' where Server = '{1}'"
    queryHostDiscovery = "select Server from esxi_credentials"
    queryMailing = "select storage,hardware,risorse,delay,last_notify from mailing_list where Server = '{0}'"
    queryConfig = "" #carico cfg (ancora da individuare) dal db
    queryDisks = "SELECT c.name, c.status, c.host from esxi_credentials inner join (SELECT * FROM storage WHERE timestamp IN ( SELECT MAX(timestamp) FROM storage GROUP BY name )) as c on c.host = esxi_credentials.Server WHERE c.host = '{0}' GROUP BY c.name"
    querySensors = "SELECT c.name,reading,type,temp_limit,c.host from esxi_credentials inner join (SELECT * FROM system_sensors WHERE timestamp IN ( SELECT MAX(timestamp) FROM system_sensors GROUP BY name )) as c on c.host = esxi_credentials.Server INNER JOIN registered_sensors ON registered_sensors.name = c.name WHERE c.host = '{0}' GROUP BY c.name"
    queryDatastores = "SELECT c.name,c.capacity,c.provisioned,c.host from esxi_credentials inner join (SELECT * FROM datastore WHERE timestamp IN ( SELECT MAX(timestamp) FROM datastore GROUP BY name )) as c on c.host = esxi_credentials.Server where c.host = '{0}' group by c.name"
    queryPerformance = "" #carico RAM;CPU e trovo il modo di verificare se per x tempo sono ancora strozzate
    queryConfiguredRam = "SELECT configured_mem from esxi_credentials inner join (SELECT * FROM vm_data WHERE timestamp IN ( SELECT MAX(timestamp) FROM vm_data GROUP BY name )) as c on c.server = esxi_credentials.Server where c.server = '{0}' group by c.name"
    queryTotalRam = "SELECT total_memory,host from host_data where host = '{0}' order by timestamp desc limit 1"
    queryPerecentage = "SELECT percentage from system order by timestamp desc limit 1"

    resultHostDiscovery = getFromDb(queryHostDiscovery)
    hosts=[]
    for x in resultHostDiscovery:
        caso = getFromDb(queryMailing.format(''.join(x)))
        if caso:
            hosts.append(''.join(x))
    
    
    disksData = []
    sensorsData = []
    datastoresData = []
    configuredRam = []
    mailingList = {}
    totalram = []
    totalPercentage = 0
    

    for x in hosts:
        temp=getFromDb(queryMailing.format(x))
        delay = temp[0][3]*3600
        last_notify = temp[0][4]
        timediff = datetime.datetime.now() - last_notify
        timediffseconds = timediff.total_seconds()
        
        if(timediffseconds > delay):
            writeDataToDb(queryStampNotify.format(datetime.datetime.now(),x))
            disksData.append(getFromDb(queryDisks.format(x)))
            sensorsData.append(getFromDb(querySensors.format(x)))
            datastoresData.append(getFromDb(queryDatastores.format(x)))
            configuredRam.append(getFromDb(queryConfiguredRam.format(x)))
            totalram = getFromDb(queryTotalRam.format(x))
            mailingList.update({x:getFromDb(queryMailing.format(x))})
            
            
    
    tempperc = getFromDb(queryPerecentage)
    if(tempperc):
        totalPercentage = tempperc[0][0]
    parsePercentage(totalPercentage)
    parseConfiguredRam(configuredRam,totalram,mailingList)
    parseDisks(disksData,mailingList)
    parseSensors(sensorsData,mailingList)
    parseDatastore(datastoresData,mailingList)
    split4mail()
    

execute()


