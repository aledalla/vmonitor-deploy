-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 30, 2019 at 03:41 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vmonitor`
--
CREATE DATABASE IF NOT EXISTS `vmonitor` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `vmonitor`;

-- --------------------------------------------------------

--
-- Table structure for table `core_config`
--

CREATE TABLE `core_config` (
  `name` varchar(20) NOT NULL,
  `path` varchar(100) NOT NULL,
  `waitfor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `core_config`
--

INSERT INTO `core_config` (`name`, `path`, `waitfor`) VALUES
('querier', '/home/vmonitor/VMonitor/collector.py', 30),
('watchdog', '/home/vmonitor/VMonitor/watchdog.py', 30);

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `name` varchar(100) NOT NULL,
  `capacity` double NOT NULL,
  `freespace` double NOT NULL,
  `provisioned` double NOT NULL,
  `timestamp` datetime NOT NULL,
  `host` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `esxi_credentials`
--

CREATE TABLE `esxi_credentials` (
  `Server` varchar(16) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `alias` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `host_data`
--

CREATE TABLE `host_data` (
  `total_cpu` bigint(20) NOT NULL,
  `total_memory` double NOT NULL,
  `effective_cpu` bigint(20) NOT NULL,
  `effective_memory` double NOT NULL,
  `overall_memory` double NOT NULL,
  `overall_cpu` bigint(20) NOT NULL,
  `overall_status` varchar(10) NOT NULL,
  `timestamp` datetime NOT NULL,
  `cpu_status` varchar(10) NOT NULL,
  `memory_status` varchar(10) NOT NULL,
  `host` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `Log`
--

CREATE TABLE `Log` (
  `Component` varchar(20) NOT NULL,
  `Error` varchar(100) NOT NULL,
  `Timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mailing_list`
--

CREATE TABLE `mailing_list` (
  `Server` varchar(20) NOT NULL,
  `storage` varchar(40) NOT NULL,
  `hardware` varchar(40) NOT NULL,
  `risorse` varchar(40) NOT NULL,
  `delay` int(11) NOT NULL,
  `last_notify` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `network`
--

CREATE TABLE `network` (
  `vmnic` varchar(20) NOT NULL,
  `tx` bigint(50) NOT NULL,
  `rx` bigint(50) NOT NULL,
  `tot` bigint(50) NOT NULL,
  `host` varchar(20) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `registered_sensors`
--

CREATE TABLE `registered_sensors` (
  `name` varchar(200) NOT NULL,
  `temp_limit` double DEFAULT NULL,
  `host` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `registered_vm`
--

CREATE TABLE `registered_vm` (
  `uuid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `storage`
--

CREATE TABLE `storage` (
  `name` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `host` varchar(16) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `system`
--

CREATE TABLE `system` (
  `percentage` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `system_sensors`
--

CREATE TABLE `system_sensors` (
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `host` varchar(16) NOT NULL,
  `timestamp` datetime NOT NULL,
  `reading` double NOT NULL,
  `unit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sys_login`
--

CREATE TABLE `sys_login` (
  `username` varchar(20) NOT NULL,
  `password` varbinary(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vm_data`
--

CREATE TABLE `vm_data` (
  `onoff` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `host_memory` double NOT NULL,
  `guest_memory` double NOT NULL,
  `storage` double NOT NULL,
  `storagefree` double NOT NULL,
  `name` varchar(50) NOT NULL,
  `vmpath` varchar(100) NOT NULL,
  `cpuusage` int(50) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `server` varchar(20) NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `configured_mem` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `web_config`
--

CREATE TABLE `web_config` (
  `refreshtime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `web_config`
--

INSERT INTO `web_config` (`refreshtime`) VALUES
(60);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `core_config`
--
ALTER TABLE `core_config`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`name`,`timestamp`,`host`);

--
-- Indexes for table `esxi_credentials`
--
ALTER TABLE `esxi_credentials`
  ADD PRIMARY KEY (`Server`);

--
-- Indexes for table `host_data`
--
ALTER TABLE `host_data`
  ADD PRIMARY KEY (`timestamp`,`host`);

--
-- Indexes for table `mailing_list`
--
ALTER TABLE `mailing_list`
  ADD PRIMARY KEY (`Server`);

--
-- Indexes for table `network`
--
ALTER TABLE `network`
  ADD PRIMARY KEY (`vmnic`,`host`,`timestamp`);

--
-- Indexes for table `registered_sensors`
--
ALTER TABLE `registered_sensors`
  ADD PRIMARY KEY (`name`,`host`);

--
-- Indexes for table `registered_vm`
--
ALTER TABLE `registered_vm`
  ADD PRIMARY KEY (`uuid`);

--
-- Indexes for table `vm_data`
--
ALTER TABLE `vm_data`
  ADD PRIMARY KEY (`timestamp`,`uuid`);


USE `mysql`;
UPDATE user SET PASSWORD=PASSWORD("°!pZçv8#§§eW") WHERE USER='root';
flush privileges;


COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
