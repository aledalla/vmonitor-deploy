<html>
    <header>
    <link rel="stylesheet" href="settings/settings.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="settings/settings.js"></script>
        <body>
        
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="watchdog.php">Watchdog</a>
            </li>
          
          
        </ul>
    </div>
    <div class="mx-auto order-0">
        <a class="navbar-brand mx-auto" href="index.php">V-Monitor @ Global settings</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="settings.php">Impostazioni</a>
            </li>
            
        </ul>
    </div>
</nav>

    <div class="semi-ex">
    <form method="post">
        <div class="block"> 
            <div class="title">
          
            <hr class="new2">

                <h4>Server collegati</h4>
            </div>
            <div class="content">
            <div class="img" style="background: url(img/serverx.svg) no-repeat center center;">
                </div>
                <div class="label">
                <div class="internal">
                <div class="btn-group" style="width:100%;">
  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Server
  </button>
  <div class="dropdown-menu" id="server">
  </div>
  <button type="button" class="btn btn-success" onclick="location.href='add_esxi.php'">Aggiungi</button>
</div>

                </div>
                </div>
            </div>
            <hr class="new2">

        </div>

        <div class="block">
            <div class="title">
            <h4>Frequenza aggiornamento GUI</h4> 
                           
            </div>
            <div class="content">
            <div class="img" style="background: url(img/gear2.svg) no-repeat center center;">
                                
                </div>
                <div class="label">
                <div class="internal">
                    <label>indicare il tempo in secondi</label>
                    <input type="text" class="form-control" id="refresh" name="refresh" placeholder="secondi">
                </div>
                </div>
            </div>
<hr class="new2">
        </div>

        <div class="block">
            <div class="title">
            <h4>Frequena interrogazione Esxi</h4>
            
            </div>
            <div class="content">
            <div class="img" style="background: url(img/downloadx.svg) no-repeat center center;">
                </div>
                <div class="label">
                <div class="internal">
                <label>indicare il tempo in secondi</label>
                    <input type="text" class="form-control" id="waitfor" name="waitfor" placeholder="secondi">
</div>
</div>
            </div>
<hr class="new2">
        </div>

        <div class="block">
            <div class="title">
            <h4>Percorso core</h4>
                
            </div>
            <div class="content">
            <div class="img" style="background: url(img/cpux.svg) no-repeat center center;">
                </div>
                <div class="label">
                <div class="internal">
                <label>indicare il percorso completo</label>
                    <input type="text" class="form-control" id="path" name="path" placeholder="path">
                    </div>
</div>
            </div>
</div>
<hr class="new2">
            <div class="block">
            <div class="title">
            <h4>Frequena controllo allarmi</h4>
            
            </div>
            <div class="content">
            <div class="img" style="background: url(img/downloadx.svg) no-repeat center center;">
                </div>
                <div class="label">
                <div class="internal">
                <label>indicare il tempo in secondi</label>
                    <input type="text" class="form-control" id="waitforwatchdog" name="waitforwatchdog" placeholder="secondi">
</div>
</div>
            </div>
<hr class="new2">
        </div>

        <div class="block">
            <div class="title">
            <h4>Percorso Watchdog</h4>
                
            </div>
            <div class="content">
            <div class="img" style="background: url(img/cpux.svg) no-repeat center center;">
                </div>
                <div class="label">
                <div class="internal">
                <label>indicare il percorso completo</label>
                    <input type="text" class="form-control" id="watchdogpath" name="watchdogpath" placeholder="path">
                    </div>
</div>
            </div>


<hr class="new2">
        </div>

        <div class="block">
            <div class="title">
            <h4>Configurazione allarmi</h4>
                
            </div>
            <div class="content">
            <div class="img" style="background: url(img/lightbulbx.svg) no-repeat center center;">
                </div>
                <div class="label">
                <div class="internal">
                <input class="btn btn-primary" type="button" value="Watchdog config" style="width:100%;" onclick="location.href='watchdog.php'">
</div>
</div>

            </div>

        </div>
        
        <button type="submit" class="btn btn-success btn-lg btn-block" name="submitted" style="margin-top:10%;">Salva configurazione</button>
        </form>
    </div>
        </body>
    </header>
</html>

<?php

$servername = "localhost";
$username = "root";
$password = "°!pZçv8#§§eW";
$dbname = "vmonitor";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT * FROM core_config"; 
$result = mysqli_query($conn, $sql);


if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        if($row['name']=='querier'){
        $formatted = "<script>fill('%s','%s')</script>";
        echo sprintf($formatted,"path",$row['path']);
        $GLOBALS['path'] = $row['path'];
        $formatted2 = "<script>fill('%s','%s')</script>";
        echo sprintf($formatted2,"waitfor",$row['waitfor']);
        $GLOBALS['waitfor'] = $row['waitfor'];
    }
    if($row['name']=='watchdog'){
        $formatted = "<script>fill('%s','%s')</script>";
        echo sprintf($formatted,"watchdogpath",$row['path']);
        $GLOBALS['path'] = $row['path'];
        $formatted2 = "<script>fill('%s','%s')</script>";
        echo sprintf($formatted2,"waitforwatchdog",$row['waitfor']);
        $GLOBALS['waitforwatchdog'] = $row['waitfor'];
    }
}
} else {
    #
}
$sql2 = "SELECT * FROM web_config";
$result2 = mysqli_query($conn, $sql2);


if (mysqli_num_rows($result2) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result2)) {
        $formatted = "<script>fill('%s','%s')</script>";
        echo sprintf($formatted,"refresh",$row['refreshtime']);
        $GLOBALS['refreshtime']=$row['refreshtime'];
    }
} else {
    #
}
$sql3 = "SELECT Server FROM esxi_credentials";
$result3 = mysqli_query($conn, $sql3);


if (mysqli_num_rows($result3) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result3)) {
        $formatted = "<script>fillServer('%s','%s')</script>";
        echo sprintf($formatted,"server",$row['Server']);
    }
} else {
    #
}
mysqli_close($conn);

if(isset($_POST['submitted'])){
$intervalgui = $_POST['refresh'];
$intervalquerier = $_POST['waitfor'];
$corepath = $_POST['path'];
$intervalalarm = $_POST['waitforwatchdog'];
$watchdogpath = $_POST['watchdogpath'];
saveConfigPath($intervalgui,$intervalquerier,$corepath,$intervalalarm,$watchdogpath);
echo "<meta http-equiv='refresh' content='0'>";
}



    function saveConfigPath($intervalgui,$intervalquerier,$corepath,$intervalalarm,$watchdogpath){
       
        $servername = "localhost";
        $username = "root";
        $password = "°!pZçv8#§§eW";
        $dbname = "vmonitor";
        
        // Create connection
        
        $conn = mysqli_connect($servername, $username, $password, $dbname);
        // Check connection
        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
        
        $sqlRefresh = "UPDATE core_config SET path = '%s', waitfor = %s where name = 'querier'";
        $sqlRefresh = sprintf($sqlRefresh,$corepath,$intervalquerier);
        
        $sqlWatchdog = "UPDATE core_config SET path= '%s', waitfor = %s where name = 'watchdog'";
        $sqlWatchdog = sprintf($sqlWatchdog,$watchdogpath,$intervalalarm);

        $sqlWebRefresh = "UPDATE web_config SET refreshtime = %s";
        $sqlWebRefresh = sprintf($sqlWebRefresh,$intervalgui);
        
        $conn->query($sqlRefresh);
        $conn->query($sqlWatchdog);
        $conn->query($sqlWebRefresh);
        
        $conn->close();
        }
?>