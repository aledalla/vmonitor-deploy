<html>
<header>
<link rel="stylesheet" href="add_esxi/add_esxi.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="add_esxi/add_esxi.js"></script>
</header>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="watchdog.php">Watchdog</a>
            </li>
         
          
        </ul>
    </div>
    <div class="mx-auto order-0">
        <a class="navbar-brand mx-auto" href="index.php">V-Monitor @ Edit Esxi host</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="settings.php">Impostazioni</a>
            </li>
            
        </ul>
    </div>
</nav>
<div class="header">
    
<div class="toast" data-delay="3000" id="toastok">
  <div class="toast-header">
    Operazione riuscita
  </div>
  <div class="toast-body">
    Salvataggio effettuato correttamente
  </div>
</div>

<div class="toast" data-delay="3000" id="taosterror">
  <div class="toast-header">
    Operazione non riuscita
  </div>
  <div class="toast-body">
    Si è verificato un problema durante il salvataggio
  </div>
</div>

</div>

</div>
<div class="external">
    
<form method="POST" class="needs-validation" novalidate>
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Server</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="ip" placeholder="Server" aria-describedby="inputGroupPrepend" required>
      <div class="invalid-feedback">
        Il campo deve essere compilato
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Alias</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="alias" placeholder="Server" aria-describedby="inputGroupPrepend" required>
      <div class="invalid-feedback">
        Il campo deve essere compilato
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="user" placeholder="Username" aria-describedby="inputGroupPrepend" required>
      <div class="invalid-feedback">
        Il campo deve essere compilato
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="pass" placeholder="Password" aria-describedby="inputGroupPrepend" required>
      <div class="invalid-feedback">
        Il campo deve essere compilato
      </div>
    </div>
  </div>
  <br>
  <div class="form-group row">
   
      <button type="submit" name="update" class="btn btn-primary btn-lg btn-block">Aggiorna</button>
  </div>
</form>
<form method="POST">
<button type="submit" name="delete" class="btn btn-danger btn-lg btn-block">Elimina</button>
</form>
</div>
</body>
</html>

<?php
    if(isset($_POST['ip']) && isset($_POST['user']) && isset($_POST['pass']) && isset($_POST['alias'])){
        $servername = "localhost";
        $username = "root";
        $password = "°!pZçv8#§§eW";
        $dbname = "vmonitor";
        
        // Create connection
        
        $conn = mysqli_connect($servername, $username, $password, $dbname);
        // Check connection
        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
        
            $sqlRefresh = "UPDATE esxi_credentials SET Server='%s', Username='%s', Password='%s', alias='%s' WHERE Server='%s'";
            $sqlRefresh = sprintf($sqlRefresh,$_POST['ip'],$_POST['user'],$_POST['pass'],$_POST['alias'], $_GET['ip']);      
    
        if (mysqli_query($conn, $sqlRefresh)) {
            echo("<script>
        $('#toastok').toast('show');
      ;</script>");
        } else {
            echo("<script>
            $('#toasterror').toast('show');
          ;</script>");
        }
        #$conn->query($sqlRefresh);
        
        $conn->close();
        echo("<script>window.location.href='index.php'</script>");

    }
    if(isset($_POST['delete'])){
      $servername = "localhost";
      $username = "root";
      $password = "°!pZçv8#§§eW";
      $dbname = "vmonitor";
      
      // Create connection
      
      $conn = mysqli_connect($servername, $username, $password, $dbname);
      // Check connection
      if (!$conn) {
          die("Connection failed: " . mysqli_connect_error());
      }
      
          $sqlRefresh = "DELETE FROM esxi_credentials WHERE Server='%s'";
          $sqlRefresh = sprintf($sqlRefresh,$_GET['ip']);      
  
      if (mysqli_query($conn, $sqlRefresh)) {
          echo("<script>
      $('#toastok').toast('show');
    ;</script>");
      } else {
          echo("<script>
          $('#toasterror').toast('show');
        ;</script>");
      }
      #$conn->query($sqlRefresh);
      
      $conn->close();
      echo("<script>window.location.href='index.php'</script>");
  
    }
    if(isset($_GET['ip'])){
      $servername = "localhost";
      $username = "root";
      $password = "°!pZçv8#§§eW";
      $dbname = "vmonitor";
      
      // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
      // Check connection
      if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
      }
      
      $sql = "SELECT Server,Username FROM esxi_credentials WHERE Server = '%s'";
      $sql = sprintf($sql,$_GET['ip']);
      $result = $conn->query($sql);
      
      if ($result->num_rows > 0) {
          // output data of each row
          while($row = $result->fetch_assoc()) {
              $formatted = "<script>document.getElementsByName('ip')[0].value = '%s';document.getElementsByName('user')[0].value = '%s';</script>";
              $formatted = sprintf($formatted,$row['Server'],$row['Username']);
              echo($formatted);
          }
      } else {
          
      }
      $conn->close();
    }
    else
    {
     #redirect visto che non ce il param
    }
 

?>