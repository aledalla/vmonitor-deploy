
function testChart(id,points,labelss,colors,text,total){
  
var ctx = document.getElementById(id).getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'doughnut',
    options: {
      title: {
          display: true,
          text: text
      },
      centerText: {
        display: true,
        text: "280"
    }
  },
    // The data for our dataset
    data : {
      datasets: [{
          data: points,
          backgroundColor: colors
      }],
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: labelss,
      
  
      
  } 
  
});
}

function addChart(style,id){
  var newDiv = document.createElement('div')
  newDiv.setAttribute('class',style)
  var newCanvas = document.createElement('canvas')
  newCanvas.setAttribute('id',id)
  newDiv.appendChild(newCanvas)
  document.getElementById('charts').appendChild(newDiv)
}
function addChart2(style,id){
  var newDiv = document.createElement('div')
  newDiv.setAttribute('class',style)
  var newCanvas = document.createElement('canvas')
  newCanvas.setAttribute('id',id)
  newDiv.appendChild(newCanvas)
  document.getElementById('charts2').appendChild(newDiv)
}

function reportedDisks(name,status){
  var newP = document.createElement('p')
  if(name!=undefined && status != undefined){
  newP.innerText = name+' è stato segnalato con lo stato '+status
  if(status=="Yellow"){
    newP.setAttribute('style','background-color:#ecf078;')
  }
  if(status=="Red"){
    newP.setAttribute('style','background-color:#f26755;')
  }
}
else{
  
  newP.innerText = "Tutti i dischi funzionano correttamente"
newP.setAttribute('style','background-color:#6af266')
}

  document.getElementById('dischi').appendChild(newP)
}


function reportedSensors(text){
  var newP = document.createElement('p')
  if(text!=undefined){
  newP.innerText = text
  newP.setAttribute('style','background-color:#ecf078;')
  }
  else{
    newP.innerText = "Tutti i sensori configurati riportano soglie rispettate"
newP.setAttribute('style','background-color:#6af266')
  }
  document.getElementById('sensori').appendChild(newP)
}



function multipleDatastore(id,name,value,valuetot){ 
  var element = document.createElement('li')
  element.innerText=name+": "+value+"/"+valuetot+" GB Utilizzati"
  document.getElementById(id).appendChild(element)

}
function multipleDatastoreProvisioned(id,name,value,valuetot){

  var element = document.createElement('li')
  element.innerText=name+": "+value+"/"+valuetot+" GB Provisioned"
  document.getElementById(id).appendChild(element)

}
function griditeminitializer(id,subid){
  var title = document.createElement('h4')
  title.innerText="Utilizzo datastores"
  var space = document.createElement('br')
  var gitem = document.createElement('div');
  gitem.setAttribute('class','grid-item')
  gitem.setAttribute('id',subid)
  document.getElementById(id).appendChild(gitem)
  document.getElementById(subid).appendChild(title)
  document.getElementById(subid).appendChild(space)
  
}

function setServerLabel(host){
  document.getElementById("serverlabel").innerText = "V-Monitor @ Host summary ("+host+")"
}