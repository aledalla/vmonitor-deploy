<html>
<header>
<script type="text/javascript" src="host_summary/host_summary.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="host_summary/host_summary.css">
<meta http-equiv="refresh" content="30" >
</header>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="watchdog.php">Watchdog</a>
            </li>
           
          
        </ul>
    </div>
    <div class="mx-auto order-0">
        <a id="serverlabel" class="navbar-brand mx-auto" href="index.php">V-Monitor @ Host summary</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="settings.php">Impostazioni</a>
            </li>
            
        </ul>
    </div>
</nav>
<div class="external">

<div class="runtime-container">
<div class="title">
<h1>Risorse a runtime</h1>
</div>
<div class="charts-container" id="charts">

 
<div class="grid-item">
<canvas id="cpu"></canvas>
</div>
<div class="grid-item">
<canvas id="mem"></canvas>
</div>
</div>

<div class="runtime-container">
<div class="title">
<h1>Risorse configurate</h1>
</div>
<div class="charts-container" id="charts2">

 
<div class="grid-item">
<div class="dishi" id="dischi">
<h4>stato dischi</h4>
</div>
<div class="sensori" id="sensori">
<h4>stato sensori</h4>
</div>
</div>
<div class="grid-item">
<canvas id="mem2"></canvas>
</div>



</div>
</div>
</body>
</html>

<?php

    $tempLabel = "<script>setServerLabel('%s')</script>";
    $tempLabel = sprintf($tempLabel,$_GET['server']);
    echo($tempLabel);

     ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
     $cpuquery = "SELECT overall_cpu,total_cpu from host_data where host = '%s' order by timestamp desc limit 1";
     $cpuresult = getDataFromDb(sprintf($cpuquery,$_GET['server']));
     if ($cpuresult->num_rows > 0) {
        // output data of each row
        while($row = $cpuresult->fetch_assoc()) {
            $data[] = $row['overall_cpu'];
            $data[] = $row['total_cpu'];
            $labels[] = 'cpu in uso';
            $labels[] = 'cpu libera';
            $colors[] = rand_color();
            $colors[] = rand_color();
        }}
     $freecpu = $data[1]-$data[0];
     $cpufinal=[];
     array_push($cpufinal,$data[0],$freecpu);
        
     $formatted = "<script>testChart('cpu',%s,%s,%s,'Occupazione globale cpu')</script>";
     $formatted = sprintf($formatted,json_encode($cpufinal),json_encode($labels),json_encode($colors));
     echo($formatted);
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
     
    $memquery = "SELECT overall_memory,total_memory from host_data where host = '%s' order by timestamp desc limit 1";
     $memresult = getDataFromDb(sprintf($memquery,$_GET['server']));
     if ($memresult->num_rows > 0) {
        // output data of each row
        while($row = $memresult->fetch_assoc()) {
            $datamem[] = $row['overall_memory'];
            $datamem[] = $row['total_memory'];
            $labelsmem[] = 'memoria in uso';
            $labelsmem[] = 'memoria libera';
        }}
     $freemem = $datamem[1]-$datamem[0];
     $memfinal=[];
     array_push($memfinal,$datamem[0],$freemem);
        
     $formatted = "<script>testChart('mem',%s,%s,%s,'Occupazione globale mem')</script>";
     $formatted = sprintf($formatted,json_encode($memfinal),json_encode($labelsmem),json_encode($colors));
     echo($formatted);

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $dsquery = "SELECT name,freespace,capacity from esxi_credentials inner join (SELECT * FROM datastore WHERE timestamp IN ( SELECT MAX(timestamp) FROM datastore GROUP BY name )) as c on c.host = esxi_credentials.Server where c.host = '%s' group by c.name";
     $dsresult = getDataFromDb(sprintf($dsquery,$_GET['server']));
     if ($dsresult->num_rows > 0) {
         if($dsresult->num_rows == 1){
        // output data of each row
        while($row = $dsresult->fetch_assoc()) {
            $datads[] = $row['capacity'];
            $datads[] = $row['freespace'];
            $labelsds[] = 'spazio utilizzato';
            $labelsds[] = 'spazio libero';
        }
     $freeds = $datads[0]-$datads[1];
     $dsfinal=[];
     array_push($dsfinal,$freeds,$datads[1]);
    
    
     $new = "<script>addChart('grid-item','ds')</script>";
     echo($new);
     $formatted = "<script>testChart('ds',%s,%s,%s,'Occupazione globale datastore')</script>";
     $formatted = sprintf($formatted,json_encode($dsfinal),json_encode($labelsds),json_encode($colors));
     echo($formatted);
    }
    if($dsresult->num_rows > 1){
       
        echo("<script>griditeminitializer('charts','ds')</script>");
        while($row = $dsresult->fetch_assoc()) {
            $formatted = "<script>multipleDatastore('ds','%s','%s','%s')</script>";
            $formatted = printf($formatted,$row['name'],$row['capacity']-$row['freespace'],$row['capacity']);
            echo($formatted);
        }
    }
}
 



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $memquery2 = "SELECT configured_mem from esxi_credentials inner join (SELECT * FROM vm_data WHERE timestamp IN ( SELECT MAX(timestamp) FROM vm_data GROUP BY name )) as c on c.server = esxi_credentials.Server where c.server = '%s' group by c.name";
    $memresult2 = getDataFromDb(sprintf($memquery2,$_GET['server']));
    $totalconfigured = 0;
    if ($memresult2->num_rows > 0) {
       
       // output data of each row
       while($row = $memresult2->fetch_assoc()) {
           $rawdata[] = $row['configured_mem'];
       }
       foreach($rawdata as $item) {
           $totalconfigured = $totalconfigured + $item;
       }
    }
    $tempcalc = $datamem[1]-$totalconfigured;
    $test1[] = $totalconfigured;
    $test1[] = $tempcalc;
    $labelsmem2[] = 'totale RAM configurata';
    $labelsmem2[] = 'totale RAM rimanente';
    $formatted2 = "<script>testChart('mem2',%s,%s,%s,'RAM configurata per le VMs')</script>";
    $formatted2 = sprintf($formatted2,json_encode($test1),json_encode($labelsmem2),json_encode($colors));
    echo($formatted2);
   
 

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     $dsquery2 = "SELECT name,provisioned,capacity from esxi_credentials inner join (SELECT * FROM datastore WHERE timestamp IN ( SELECT MAX(timestamp) FROM datastore GROUP BY name )) as c on c.host = esxi_credentials.Server where c.host = '%s' group by c.name";
     $dsresult2 = getDataFromDb(sprintf($dsquery2,$_GET['server']));
     if ($dsresult2->num_rows > 0) {
         if($dsresult2->num_rows == 1){
        // output data of each row
        while($row = $dsresult2->fetch_assoc()) {
            $datads2[] = $row['provisioned'];
            $datads2[] = $row['capacity'];
            $labelsds2[] = 'spazio provisioned';
            $labelsds2[] = 'spazio disponibile provisioning';
        }
    $datads2[1] = $datads2[1]-$datads2[0];
     $new = "<script>addChart2('grid-item','ds2')</script>";
     echo($new);
     $formatted2 = "<script>testChart('ds2',%s,%s,%s,'Occupazione globale datastore')</script>";
     $formatted2 = sprintf($formatted2,json_encode($datads2),json_encode($labelsds2),json_encode($colors));
     echo($formatted2);
    }
    if($dsresult2->num_rows > 1){
        echo("<script>griditeminitializer('charts2','ds2')</script>");
        while($row = $dsresult2->fetch_assoc()) {
            $formatted = "<script>multipleDatastoreProvisioned('ds2','%s','%s','%s')</script>";
            $formatted = printf($formatted,$row['name'],$row['provisioned'],$row['capacity']);
            echo($formatted);
        }
    }
}
 




    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    $sql = "SELECT c.name, c.status from esxi_credentials inner join (SELECT * FROM storage WHERE timestamp IN ( SELECT MAX(timestamp) FROM storage GROUP BY name )) as c on c.host = esxi_credentials.Server WHERE c.host = '%s' GROUP BY c.name";
    $sql = sprintf($sql,$_GET['server']);
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
        
        // output data of each row
       $everythingOk = true;
        while($row = $result->fetch_assoc()) {
            if($row['status'] != 'Green'){
                $everythingOk = false;
                $formatted = "<script>reportedDisks('%s','%s')</script>";
                $formatted = sprintf($formatted,$row['name'],$row['status']);
                echo($formatted);
                
            }
        }
        if($everythingOk == True){
            echo("<script>reportedDisks()</script>");
                
        }
    } 
    $sql = "SELECT c.name,reading,type,temp_limit from esxi_credentials inner join (SELECT * FROM system_sensors WHERE timestamp IN ( SELECT MAX(timestamp) FROM system_sensors GROUP BY name )) as c on c.host = esxi_credentials.Server INNER JOIN registered_sensors ON registered_sensors.name = c.name WHERE c.host = '%s' GROUP BY c.name";
    $sql = sprintf($sql,$_GET['server']);
   
    $result = getDataFromDb($sql);
    if ($result->num_rows > 0) {
        
        // output data of each row
       $everythingOk = true;
        while($row = $result->fetch_assoc()) {
            if($row['type'] == 'temperature' and $row['temp_limit'] < $row['reading']){
                $everythingOk = false;
                $formatted = "<script>reportedSensors('%s')</script>";
                $formatted = sprintf($formatted,$row['name'] . " ha superato il limite di allerta e la sua temperatura attuale è di " . $row['reading'] . " gradi");
                echo($formatted);
            }
            if($row['type'] == 'power' and $row['reading'] == 0){
                $everythingOk = false;
                $formatted = "<script>reportedSensors('%s')</script>";
                $formatted = sprintf($formatted,$row['name'] . "risulta non connesso");
                echo($formatted);
            }
            if($row['type'] == 'fan' and $row['reading'] == 0){
                $everythingOk = false;
                $formatted = "<script>reportedSensors('%s')</script>";
                $formatted = sprintf($formatted,$row['name'] . "risulta non sta funzionando");
                echo($formatted);
            }
        }
        if($everythingOk == true){
            echo("<script>reportedSensors()</script>");
        }
    } 

    
    
    
    function rand_color() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

function getDataFromDb($sql){
    $servername = "localhost";
    $username = "root";
    $password = "°!pZçv8#§§eW";
    $dbname = "vmonitor";
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        echo("errore durante la connessione al db");
        die("Connection failed: " . $conn->connect_error);
    }
    $result = $conn->query($sql);
    return $result;
    $conn->close();
}
?>