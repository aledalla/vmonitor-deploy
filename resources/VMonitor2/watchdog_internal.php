<html>
<header>
<link rel="stylesheet" href="watchdog_internal/watchdog_internal.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="watchdog_internal/watchdog_internal.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</header>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="watchdog.php">Watchdog</a>
            </li>
           
          
        </ul>
    </div>
    <div class="mx-auto order-0">
        <a class="navbar-brand mx-auto" href="index.php">V-Monitor @ Configurazione allarmi su Host</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="settings.php">Impostazioni</a>
            </li>
            
        </ul>
    </div>
</nav>



<div class="toast" data-delay="3000" id="toastok">
  <div class="toast-header">
    Operazione riuscita
  </div>
  <div class="toast-body">
    Da ora Whatchdog controllerà anche questo
  </div>
</div>

<div class="toast" data-delay="3000" id="taosterror">
  <div class="toast-header">
    Operazione non riuscita
  </div>
  <div class="toast-body">
    Si è verificato un problema durante il salvataggio
  </div>
</div>

</div>


<div class="external">
<div class="title">
        <h1>Controllo sensori</h1>
</div>

<div class="sensor-config">
        
<form method="POST">
<div class="content" id="sensors">
<label for="sel1">Seleziona sensore da configurare</label>
  <select class="form-control" id="sel1" name="sensor" onchange="selected('sensor')">
  <option value="" selected disabled hidden>Scegli...</option>
  </select>
  <br>
  <!-- <label for="sel1a">Seleziona soglia di allarme</label> -->
   <!-- <input type="range" class="custom-range" min="0" max="5" step="0.5" id="customRange3"> -->
</div> 
</form>
    
<form method = "POST">
<div class="content" id="sensorremove">
<label for="selremove">Seleziona sensore da rimuovere</label>
  <select class="form-control" id="selremove" name="sensorremove" onchange="selected('sensorremove')">
  <option value="" selected disabled hidden>Scegli...</option>
  </select>
  <br>
</div>
</div>
</form>
<br><br>

<div class="title">
<h1>Notifiche</h1>
</div>
<br>
<div class="sensor-config" id="alarms">
<form method="POST">
<br>
<div class="form-check">
  <input class="form-check-input" type="radio" name="radio" id="exampleRadios1" onclick="handleClick(this.value);" value="single" checked>
  <label class="form-check-label" for="exampleRadios1">
    Utilizza un indirizzo unico per tutti gli allarmi
  </label>
</div>
<div class="form-check" id="test">
  <input class="form-check-input" type="radio" name="radio" id="exampleRadios2" onclick="handleClick(this.value);" value="multi">
  <label class="form-check-label" for="exampleRadios2">
    Utilizza più indirizzi
  </label>
</div>

</div>
</div>
</form>

</body>
</html>

<?php
   
    if(!empty($_GET['server'])){    
      
      
      $servername = "localhost";
      $username = "root";
      $password = "°!pZçv8#§§eW";
      $dbname = "vmonitor";
      
      // Create connection
      
      $conn = mysqli_connect($servername, $username, $password, $dbname);
      // Check connection
      if (!$conn) {
          die("Connection failed: " . mysqli_connect_error());
      }
      $sql = "select * from mailing_list where Server = '%s'";
      $sql = sprintf($sql,$_GET['server']);
      $result = mysqli_query($conn, $sql);
     
      if (mysqli_num_rows($result) > 0) {
        echo("<script>removeMailing()</script>");
     
        
    } 
    if(isset($_POST['btnerase'])){
      $servername = "localhost";
      $username = "root";
      $password = "°!pZçv8#§§eW";
      $dbname = "vmonitor";
      
      // Create connection
      
      $conn = mysqli_connect($servername, $username, $password, $dbname);
      // Check connection
      if (!$conn) {
          die("Connection failed: " . mysqli_connect_error());
      }
      $sql = "DELETE from mailing_list WHERE Server='%s'";
      $sql = sprintf($sql,$_GET['server']);

      if ($conn->query($sql) === TRUE) {
         
      } else {
          
      }

      $conn->close();
      echo("<script>window.location.href='index.php'</script>");
    }
        
if(isset($_POST['single'])){
  $servername = "localhost";
    $username = "root";
    $password = "°!pZçv8#§§eW";
    $dbname = "vmonitor";
    
    // Create connection
    
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "select Server from mailing_list where Server = '%s'";
    $sql = sprintf($sql,$_GET['server']);
    $result = mysqli_query($conn, $sql);
   
    if (mysqli_num_rows($result) > 0) {
      $sqlRefresh = "update mailing_list set storage = '%s',hardware = '%s',risorse = '%s', delay = '%s' where Server = '%s'";
      $sqlRefresh = sprintf($sqlRefresh,$_POST['single'],$_POST['single'],$_POST['single'],$_POST['delay'],$_GET['server']);
      echo($sqlRefresh);
      
  } else {
    $sqlRefresh = "insert into mailing_list (Server,storage,hardware,risorse,delay,last_notify) values ('%s','%s','%s','%s','%s','1990-01-01 00:00:00')";
    $sqlRefresh = sprintf($sqlRefresh,$_GET['server'],$_POST['single'],$_POST['single'],$_POST['single'],$_POST['delay']);
   echo($sqlRefresh);

    
  }
   
    if (mysqli_query($conn, $sqlRefresh)) {
      echo("<script>
  $('#toastok').toast('show');
  ;</script>");
  } else {
      echo("<script>
      $('#toasterror').toast('show');
    ;</script>");
  }
    
    $conn->close();
    echo("<script>window.location.href='index.php'</script>");

}
if(isset($_POST['storage'])){
  $servername = "localhost";
    $username = "root";
    $password = "°!pZçv8#§§eW";
    $dbname = "vmonitor";
    
    // Create connection
    
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "select Server from mailing_list where Server = '%s'";
    $sql = sprintf($sql,$_GET['server']);
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
      $sqlRefresh = "update mailing_list set storage = '%s',hardware = '%s',risorse = '%s',delay = '%s' where Server = '%s'";
      $sqlRefresh = sprintf($sqlRefresh,$_POST['storage'],$_POST['hardware'],$_POST['risorse'],$_POST['delay'],$_GET['server']);
      
  } else {
    $sqlRefresh = "insert into mailing_list (Server,storage,hardware,risorse,delay,last_notify) values ('%s','%s','%s','%s','%s','1990-01-01 00:00:00')";
    $sqlRefresh = sprintf($sqlRefresh,$_GET['server'],$_POST['storage'],$_POST['hardware'],$_POST['risorse'],$_POST['delay']);
    
  }
    

    if (mysqli_query($conn, $sqlRefresh)) {
        echo("<script>
    $('#toastok').toast('show');
  ;</script>");
    } else {
        echo("<script>
        $('#toasterror').toast('show');
      ;</script>");
    }

    
    $conn->close();
    echo("<script>window.location.href='index.php'</script>");

}

        

#-------------------------------------------------------------------------------
        if(isset($_POST['sensorremove'])){
            $servername = "localhost";
    $username = "root";
    $password = "°!pZçv8#§§eW";
    $dbname = "vmonitor";
    
    // Create connection
    
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    
    $sqlRefresh = "delete from registered_sensors where name = '%s'";
    $sqlRefresh = sprintf($sqlRefresh,$_POST['sensorremove']);
   

    if (mysqli_query($conn, $sqlRefresh)) {
        echo("<script>
    $('#toastok').toast('show');
  ;</script>");
    } else {
        echo("<script>
        $('#toasterror').toast('show');
      ;</script>");
    }

    
    $conn->close();
        }

       
        if(isset($_POST['sensor'])){
            $servername = "localhost";
    $username = "root";
    $password = "°!pZçv8#§§eW";
    $dbname = "vmonitor";
    
    // Create connection
    
    $conn2 = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn2) {
        die("Connection failed: " . mysqli_connect_error());
    }
    if(isset($_POST['seeker'])){
        $seekOk = $_POST['seeker'];
    }
    else{
        $seekOk = 'NULL';
    }
    $sqlRefresh2 = "insert into registered_sensors (name,temp_limit,host) values('%s',%s,'%s')";
    $sqlRefresh2 = sprintf($sqlRefresh2,$_POST['sensor'],$seekOk,$_GET['server']);
  
    if (mysqli_query($conn2, $sqlRefresh2)) {
        echo("<script>
    $('#toastok').toast('show');
  ;</script>");
    } else {
        echo("<script>
        $('#toasterror').toast('show');
      ;</script>");
    }
    
    
    #$conn->query($sqlRefresh);
    
    $conn2->close();
        }
        $servername = "localhost";
        $username = "root";
        $password = "°!pZçv8#§§eW";
        $dbname = "vmonitor";
        
        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $dbname);
        // Check connection
        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
        #query con max sbagliata
        $sql = "SELECT MAX(timestamp),name,type,reading,unit FROM system_sensors where host = '%s' and name not in (select name from registered_sensors) group by name";
        $sql = sprintf($sql,$_GET['server']);
        $result = mysqli_query($conn, $sql);

       


        $sqlSensorRemove = "select name from registered_sensors where host = '%s'";
        $sqlSensorRemove = sprintf($sqlSensorRemove,$_GET['server']);
        $resultSensorRemove = $conn->query($sqlSensorRemove);

        
        
        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {
                $formatted = "<script>populateSensorsList('%s')</script>";
                echo sprintf($formatted,$row['name']);
            }
        } else {
            #
        }
      
        
        if (mysqli_num_rows($resultSensorRemove) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($resultSensorRemove)) {
                $formatted = "<script>populateSensorsRemove('%s')</script>";
                echo sprintf($formatted,$row['name']);

            }
        } else {
            #

        }
       
        mysqli_close($conn);
    }
    
    else{
        header('Location: watchdog.php');
    }

    function fix($sql){
      $servername = "localhost";
      $username = "root";
      $password = "°!pZçv8#§§eW";
      $dbname = "vmonitor";
      
      // Create connection
      
      $conn = mysqli_connect($servername, $username, $password, $dbname);
      // Check connection
      if (!$conn) {
          die("Connection failed: " . mysqli_connect_error());
      }
      if (mysqli_query($conn, $sql)) {
        
      } else {
          
      }
     
     
          }
          
?>