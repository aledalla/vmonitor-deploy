#!/usr/bin/env python
# VMware vSphere Python SDK
# Copyright (c) 2008-2013 VMware, Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re
import atexit

from pyVim import connect
from pyVmomi import vmodl
from pyVmomi import vim
import mysql.connector
import datetime

import tools.cli as cli


class Log:
    def __init__(self, onoff,status, memory,guestMemory,storage,storagefree,name,vmpath,cpuusage,ip,server, uuid,configured_mem):
        self.onoff = onoff
        self.status = status
        self.memory = memory
        self.guestMemory = guestMemory
        self.storage = storage
        self.storagefree = storagefree
        self.name = name
        self.vmpath=vmpath
        self.cpuusage = cpuusage
        self.ip = ip
        self.server = server
        self.uuid = uuid
        self.configured_mem=configured_mem

class Storage:
    def __init__(self,name,status ,host, timestamp):
        self.name=name
        self.status = status
        self.host = host
        self.timestamp = timestamp

class Sensors:
    def __init__(self, name, tipo, reading, unit, host, timestamp):
        self.name = name
        self.tipo = tipo
        self.reading = reading
        self.unit = unit
        self.host = host
        self.timestamp = timestamp

class Datastore:
    def __init__(self,name,capacity,freespace,provisioned,date,host):
            self.name = name
            self.capacity = capacity
            self.freespace = freespace
            self.provisioned = provisioned
            self.date = date
            self.host = host
class Host:
    def __init__(self,total_cpu,total_memory,effective_cpu,effective_memory,overall_memory,overall_cpu,overall_status,timestamp,cpu_status,memory_status,host):
            self.total_cpu = total_cpu
            self.total_memory = total_memory
            self.effective_cpu = effective_cpu
            self.effective_memory = effective_memory
            self.overall_memory = overall_memory
            self.overall_cpu = overall_cpu
            self.overall_status = overall_status
            self.timestamp = timestamp
            self.cpu_status = cpu_status
            self.memory_status = memory_status
            self.host = host
class Network:
    def __init__(self,vmnic,tx,rx,usage):
        self.vmnic = vmnic
        self.tx = tx
        self.rx = rx
        self.usage = usage

def convert(value):
    b = float(value)
    kb = float(1024)
    gb = float(kb ** 3)
    return '{0:.2f}'.format(value/gb)


def storeVmOnDb(log,storage,sensors,datastore,hostClass,schede):
    mydb = mysql.connector.connect(host="localhost",user="root",passwd="°!pZçv8#§§eW",database="vmonitor")
    mycursor = mydb.cursor()
    
    if(log != None):
        
        if(log.onoff != "poweredOn"):
            log.cpuusage = 0
            log.memory = 0
            log.guestMemory = 0
        if(log.memory == None or log.guestMemory == None):
            log.memory = 0
            log.guestMemory = 0

        formatteedValues = "'%s', '%s', '%s', '%s',%s, %s, '%s', '%s', %s, '%s', '%s','%s','%s','%s'" % (log.onoff,log.status,log.memory,log.guestMemory,convert(log.storage),convert(log.storagefree),log.name,log.vmpath,log.cpuusage,log.ip,datetime.datetime.now(),log.server,log.uuid,log.configured_mem/1000)
        sql = "INSERT INTO vm_data (onoff,status,host_memory,guest_memory,storage,storagefree,name,vmpath,cpuusage,ip,timestamp,server,uuid,configured_mem) VALUES (%s)" % (formatteedValues)
        val = (formatteedValues)
        
    
    if(storage != None):
        formatteedValues = "'%s', '%s', '%s', '%s'" % (storage.name, storage.status, storage.host, datetime.datetime.now())
        sql = "INSERT INTO storage (name,status,host,timestamp) VALUES (%s)" % (formatteedValues)
    
    if(sensors != None):
        if(sensors.tipo == "temperature"):
            sensors.reading = sensors.reading/100
        formatteedValues = "'%s', '%s', '%s', '%s', '%s', '%s'" % (sensors.name, sensors.tipo, sensors.reading , sensors.unit, sensors.host, datetime.datetime.now())
        sql = "INSERT INTO system_sensors (name,type,reading,unit,host,timestamp) VALUES (%s)" % (formatteedValues)

    if(datastore != None):
       
        formattedValues = "'%s',%s,%s,%s,'%s','%s'" % (datastore.name,convert(datastore.capacity),convert(datastore.freespace),convert(datastore.provisioned),datastore.date,datastore.host)

        sql = "INSERT INTO datastore (name,capacity,freespace,provisioned,timestamp,host) VALUES (%s)" % (formattedValues)
    
       
    
    if(hostClass!=None):
        formattedValues = "%s,%s,%s,%s,%s,%s,'%s','%s','%s','%s','%s'" % (hostClass.total_cpu,convert(hostClass.total_memory),hostClass.effective_cpu,convert(hostClass.effective_memory),convert(hostClass.overall_memory*1000),hostClass.overall_cpu,hostClass.overall_status,hostClass.timestamp,hostClass.cpu_status,hostClass.memory_status,hostClass.host)
    
       
        sql = "INSERT INTO host_data (total_cpu,total_memory,effective_cpu,effective_memory,overall_memory,overall_cpu,overall_status,timestamp,cpu_status,memory_status,host) VALUES (%s)" % (formattedValues)
    
    if(schede != None):
        temp = schede.split(",")
        for x in temp:
            formattedValues = "'%s',%s,%s,%s,'%s','%s'" % (temp[0],temp[1],temp[2],temp[3],temp[4],datetime.datetime.now())
            sql = "INSERT INTO network (vmnic,tx,rx,tot,host,timestamp) VALUES (%s)" % (formattedValues)
            mycursor.execute(sql)
            mydb.commit()
            return
    
    mycursor.execute(sql)
    mydb.commit()

    



def get_args():
    parser = cli.build_arg_parser()
    parser.add_argument('-f', '--find',
                        required=False,
                        action='store',
                        help='String to match VM names')
    parser.add_argument('-n', '--name', required=False,
                        help="Name of the Datastore.")                   
    args = parser.parse_args()

    return cli.prompt_for_password(args)


def print_vm_info(virtual_machine):
   
    summary = virtual_machine.summary
    stats = summary.quickStats

    

    if summary.guest is not None:
        ip_address = summary.guest.ipAddress
    else:
        ip_address = 'ND'    
    args = get_args()


    log = Log(summary.runtime.powerState,summary.overallStatus,stats.hostMemoryUsage,stats.guestMemoryUsage,summary.storage.committed,summary.storage.uncommitted,summary.config.name,summary.config.vmPathName,stats.overallCpuUsage,ip_address,args.host,summary.config.uuid,summary.config.memorySizeMB)
    storeVmOnDb(log,None,None,None,None,None)

def get_obj(content, vim_type, name=None):
    obj = None
    container = content.viewManager.CreateContainerView(
        content.rootFolder, vim_type, True)
    if name:
        for c in container.view:
            if c.name == name:
                obj = c
                return [obj]
    else:
        return container.view

def main():
    
    args = get_args()

    try:
        if args.disable_ssl_verification:
            service_instance = connect.SmartConnectNoSSL(host=args.host,
                                                         user=args.user,
                                                         pwd=args.password,
                                                         port=int(args.port))
        else:
            service_instance = connect.SmartConnect(host=args.host,
                                                    user=args.user,
                                                    pwd=args.password,
                                                    port=int(args.port))

        atexit.register(connect.Disconnect, service_instance)

        content = service_instance.RetrieveContent()

        host = service_instance.content.searchIndex.FindByIp(None,args.host,False)

        perfManager = content.perfManager

        container = content.rootFolder
        viewType = [vim.HostSystem]
        recursive = True

        containerView = content.viewManager.CreateContainerView(container,
                                                                viewType,
                                                                recursive)
        children = containerView.view

        counterInfo = {}
        for c in perfManager.perfCounter:
            fullName = c.groupInfo.key + "." + c.nameInfo.key + "." + c.rollupType
            counterInfo[fullName] = c.key

        for child in children:
        # Get all available metric IDs for this VM
            counterIDs = [m.counterId for m in
                        perfManager.QueryAvailablePerfMetric(entity=child)]
        
        myList = [196614,196615,196609,5,65611]
        myCheckedList = []
       
        for x in myList:
            if(x in counterIDs):
                myCheckedList.append(x)
        # Using the IDs form a list of MetricId
        # objects for building the Query Spec
        metricIDs = [vim.PerformanceManager.MetricId(counterId=c,
                                                     instance="*")
                     for c in myCheckedList]
        
        # Build the specification to be used
        # for querying the performance manager
        spec = vim.PerformanceManager.QuerySpec(maxSample=1,
                                                entity=child,
                                                metricId=metricIDs)
        # Query the performance manager
        # based on the metrics created above
        result = perfManager.QueryStats(querySpec=[spec])
        # Loop through the results and print the output
        output = ""
        data = {}
        real_host_data = {}
       
        for r in result:
            output += "name:        " + child.summary.config.name + "\n"
            tx = {}
            rx = {}
            usage = {}

            for val in result[0].value:
                
                
                counterinfo_k_to_v = list(counterInfo.keys())[
                    list(counterInfo.values()).index(val.id.counterId)]
                    
                
                if val.id.instance == '':
                    if(counterinfo_k_to_v=='cpu.usagemhz.average' or counterinfo_k_to_v=='mem.consumed.average'):
                        real_host_data.update({counterinfo_k_to_v:val.value[0]})
                    
                    else:
                        output += "%s: %s\n" % (
                            counterinfo_k_to_v, str(val.value[0]))
                        if(counterinfo_k_to_v == 'net.transmitted.average'):
                            tx.update({"global":str(val.value[0])})
                        if(counterinfo_k_to_v == 'net.received.average'):
                            rx.update({"global":str(val.value[0])})
                        if(counterinfo_k_to_v == 'net.usage.average'):
                            usage.update({"global":str(val.value[0])})
                        
                else:
                    output += "%s (%s): %s\n" % (
                        counterinfo_k_to_v, val.id.instance, str(val.value[0]))
                    if(counterinfo_k_to_v == 'net.transmitted.average'):
                        tx.update({val.id.instance:str(val.value[0])})
                    if(counterinfo_k_to_v == 'net.received.average'):
                        rx.update({val.id.instance:str(val.value[0])})
                    if(counterinfo_k_to_v == 'net.usage.average'):
                        usage.update({val.id.instance:str(val.value[0])})
                
            data.update({"tx":tx})
            data.update({"rx":rx})
            data.update({"usage":usage})

            
            listaTx = data['tx']
            listaRx = data['rx']
            listaUsage = data['usage']

            listaSchede = []

            for schedaDiReteTxKey in listaTx:
                nometx = schedaDiReteTxKey
                valuetx = listaTx[schedaDiReteTxKey]
                for schedaDiReteRx in listaRx:
                    if nometx == schedaDiReteRx:
                        nomerx = listaRx[schedaDiReteRx]
                        break
                for schedaDiReteUsage in listaUsage:
                    if nometx == schedaDiReteUsage:
                        nomeusage = listaUsage[schedaDiReteUsage]
                        break
                
                record = nometx + ", " + valuetx + ", " + nomerx + ", " + nomeusage + ", " + args.host
                listaSchede.append(record)
            for x in listaSchede:
                storeVmOnDb(None,None,None,None,None,x)


        powerSensorsHealth = host.runtime.healthSystemRuntime.systemHealthInfo.numericSensorInfo
        attachedSensorsStorage = host.runtime.healthSystemRuntime.hardwareStatusInfo.storageStatusInfo

        
        

        for i in powerSensorsHealth:
            if(i.healthState!='Unknown' and i.sensorType!='other' and 'Other' not in i.name and 'Software' not in i.sensorType):
                sensors = Sensors(i.name, i.sensorType,i.currentReading,i.baseUnits,args.host,datetime.datetime.now())
                storeVmOnDb(None,None,sensors,None,None,None)

      
        for i in attachedSensorsStorage:
            if(i.status.key!='Unknown' and 'Disk' in i.name):
                storage = Storage(i.name, i.status.key, args.host, datetime.datetime.now())
                storeVmOnDb(None,storage,None,None,None,None)

        ds_obj_list = get_obj(content, [vim.Datastore], args.name)
        for ds in ds_obj_list:
            summary = ds.summary
            ds_capacity = summary.capacity
            ds_freespace = summary.freeSpace
            ds_uncommitted = summary.uncommitted if summary.uncommitted else 0
            ds_provisioned = ds_capacity - ds_freespace + ds_uncommitted
            datastore = Datastore(summary.name,ds_capacity,ds_freespace,ds_provisioned,datetime.datetime.now(),args.host)
            storeVmOnDb(None,None,None,datastore,None,None)

        cpuStatus = host.runtime.healthSystemRuntime.hardwareStatusInfo.cpuStatusInfo
        memoryStatus = host.runtime.healthSystemRuntime.hardwareStatusInfo.memoryStatusInfo

        cpuKey = ''
        memoryKey = ''

        for i in cpuStatus:
            cpuKey = i.status.key
        
        for i in memoryStatus:
            memoryKey = i.status.key

        def print_res_info(data):
            
            memoryOverallUsage = real_host_data['mem.consumed.average']
            cpuOverallUsage = real_host_data['cpu.usagemhz.average']
            overallStatus = data.resourcePool.runtime.overallStatus
            totalCpu = data.summary.totalCpu
            totalMemory = data.summary.totalMemory
            effectiveCpu = data.summary.effectiveCpu
            effectiveMemory = data.summary.effectiveMemory

            hostClass = Host(totalCpu,totalMemory,effectiveCpu,effectiveMemory,memoryOverallUsage,cpuOverallUsage,overallStatus,datetime.datetime.now(),cpuKey,memoryKey,args.host)
            storeVmOnDb(None,None,None,None,hostClass,None)

        container = content.rootFolder  # starting point to look into
        viewType = [vim.VirtualMachine]  # object types to look for
        recursive = True  # whether we should look into it recursively
        containerView = content.viewManager.CreateContainerView(
            container, viewType, recursive)

        ######################################
        viewType2 = [vim.ComputeResource]  # object types to look for
        containerView2 = content.viewManager.CreateContainerView(
            container, viewType2, recursive)

        children2 = containerView2.view

        if args.find is not None:
            pat = re.compile(args.find, re.IGNORECASE)
        for child in children2:
            if args.find is None:
                print_res_info(child)
            else:
                if pat.search(child.summary.config.name) is not None:
                    print_res_info(child)



        ######################################

        children = containerView.view
        if args.find is not None:
            pat = re.compile(args.find, re.IGNORECASE)
        for child in children:
            if args.find is None:
                print_vm_info(child)
            else:
                if pat.search(child.summary.config.name) is not None:
                    print_vm_info(child)

    except:
        print("Connessione al server Esxi ("+args.host+") fallita")
        return -1

    return 0


# Start program
if __name__ == "__main__":
    main()
